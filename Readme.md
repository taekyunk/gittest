% Readme
%
%

* Set up vim plugins managed by Pathogen
* Setup for windows
* To use this under linux,
    1. clone this repository
    2. Rename folder from 'vimfiles' to '.vim'
    3. create .vimrc from _vimrc

    * Since mostly windows _vimrc is updated, create linux version .vimrc when necessary

    dos2unix -n _vimrc .vimrc

    * Or, create a symbolic link

    ln -s _vimrc .vimrc

* Based on Vim 7.4
----
* Added new comment here as someone else
* Made edits here independently!
* separate additions


