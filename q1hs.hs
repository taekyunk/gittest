{-# OPTIONS_GHC -Wall #-}
-- brute force version of algorithm

import           Data.List
import           Control.Applicative
import qualified Data.Set as Set
{-import qualified Data.IntMap as Map-}

readData :: FilePath -> IO [Integer]
readData fp = do
    content <- readFile fp
    let result = map read . lines $ content
    return result

countTwoSum :: FilePath -> IO Info
countTwoSum fp = do
    ys <- (nub . sort) <$> readData fp
    let result = findTwoSum ys Set.empty
    return result

inRange :: Integer -> Bool
inRange x = x >= minK && x <= maxK where
    minK = -10000
    maxK = 10000

type Info = Set.Set Integer

check :: Integer -> Integer -> Info -> Info
check x y db =  case inRange (x+y) of
    True -> Set.insert (x+y) db
    False -> db

findTwoSum :: [Integer] -> Info -> Info
findTwoSum [] db = db
findTwoSum [_] db = db
findTwoSum [x,y] db = check x y db
findTwoSum (x:xs) db = Set.union (run x xs db) (findTwoSum xs db)

run :: Integer -> [Integer] -> Info -> Info
run y ys db  = result where
    ys' = dropWhile (\z -> (not . inRange) (z + y)) ys
    ys'' = takeWhile (\z -> inRange (z + y)) ys'
    result = Set.union db (Set.fromList $ map (+y) ys'')

main :: IO ()
main = do
    result <- countTwoSum "2sum.txt"
    {-result <- countTwoSum "test111.txt"-}
    putStrLn $ "Size is " ++ show (Set.size result)



{-type ListZipper a = ([a], [a], [a])-}

{-[>data ListZipper a = ListZipper { past :: [a]<]-}
                               {-[>, current :: [a]<]-}
                               {-[>, future :: [a] <]-}
                               {-[>} deriving (Show, Eq, Ord)<]-}

{-data Direction = L | R-}

{--- increase the left end of the main element-}
{-increaseLowerUntil :: (a -> Bool) -> ListZipper a -> ListZipper a-}
{-increaseLowerUntil _ z@(_, [], _)  = z-}
{-increaseLowerUntil cond z@(ls, (m:ms), rs) -}
    {-| cond m = z-}
    {-| otherwise = increaseLowerUntil cond (m:ls, ms, rs)-}

{-increaseUpperUntil :: (a -> Bool) -> ListZipper a -> ListZipper a-}
{-increaseUpperUntil _ z@(_, _, []) = z-}
{-increaseUpperUntil cond z@(ls, ms, (r:rs))-}
    {-| cond r = z-}
    {-| otherwise = increaseUpperUntil cond (ls, ms++[r], rs)-}

{-decreaseUpperUntil :: (a -> Bool) -> ListZipper a -> ListZipper a-}
{-decreaseUpperUntil _ z@(_, _, []) = z-}
{-decreaseUpperUntil cond z@(ls, ms, rs)-}
    {-| length ms < 1 = z-}
    {-| otherwise = case cond (last ms) of-}
        {-True -> z-}
        {-False -> decreaseUpperUntil cond (ls, init ms, last ms:rs)-}


{-readDataSorted :: FilePath -> IO [Integer]-}
{-readDataSorted fp = do-}
    {-content <- readFile fp-}
    {-let result = nub . sort . map read . lines $ content-}
    {-return result-}



